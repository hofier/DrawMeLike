#include "DMLWidget.h"

using namespace std;

void ColorClamp(short& r, short& g, short& b, short& a)
{
	if (r > 255) r = 255;
	if (g > 255) g = 255;
	if (b > 255) b = 255;
	if (a > 255) a = 255;
	if (r < 0) r = 0;
	if (g < 0) g = 0;
	if (b < 0) b = 0;
	if (a < 0) a = 0;
}

void Angle2RGBA(float angle, short& r, short& g, short& b, short& a)
{
	r = 0;
	g = 0;
	b = 0;
	a = 255;
	if (angle >= 0 && angle <= 120)
		r += 255 - 255 / 60 * abs(angle - 60);
	if (angle >= 120 && angle <= 240)
		g += 255 - 255 / 60 * abs(angle - 180);
	if (angle >= 240 && angle <= 360)
		b += 255 - 255 / 60 * abs(angle - 300);
	if (angle >= 60 && angle <= 180)
	{
		r += 255 - 255 / 60 * abs(angle - 120);
		g += 255 - 255 / 60 * abs(angle - 120);
	}
	if (angle >= 180 && angle <= 300)
	{
		g += 255 - 255 / 60 * abs(angle - 240);
		b += 255 - 255 / 60 * abs(angle - 240);
	}
	if (angle >= 300)
	{
		b += 255 - 255 / 60 * abs(angle - 360);
		r += 255 - 255 / 60 * abs(angle - 360);
	}
	if (angle <= 60)
	{
		r += 255 / 60 * abs(angle - 60);
		b += 255 / 60 * abs(angle - 60);
	}
	ColorClamp(r, g, b, a);
}

void Dist2RGBA(float perc_dist, short &r, short& g, short& b, short& a)
{
	if (perc_dist < 0.1)
	{
		a = 0;
	}
	if (perc_dist >= 0.1 && perc_dist < 0.4)
	{
		a -= 255 * (0.4 - perc_dist) / 0.3;
	}
	if (perc_dist > 0.4 && perc_dist < 0.8)
	{
		float absval = 0.2 - abs(perc_dist - 0.6);
		r += absval / 0.2 * 255;
		g += absval / 0.2 * 255;
		b += absval / 0.2 * 255;
	}
	if (perc_dist >= 0.8)
	{
		float val = 1.0f - (1.0f - perc_dist) / 0.2f;
		r -= val * 255;
		g -= val * 255;
		b -= val * 255;
	}
	ColorClamp(r, g, b, a);
}

void DMLWidget::GenerateColorPalette(int radius)
{
	short r, g, b, a;
	color_palette = QImage(radius * 2, radius * 2, QImage::Format_ARGB32);
	color_palette.fill(Qt::transparent);
	for (int i = 0; i < radius * 2; i++)
	{
		for (int j = 0; j < radius * 2; j++)
		{
			float dist = sqrt(pow(i - radius, 2) + pow(j - radius, 2));
			if (dist <= radius)
			{
				float angle = atan2f(i - radius, j - radius);
				angle = (angle > 0 ? angle : (2 * 3.14 + angle)) * 360 / (2 * 3.14);
				Angle2RGBA(angle, r, g, b, a);
				Dist2RGBA(dist/radius, r, g, b, a);
				color_palette.setPixel(i, j, RGB2Uint(r, g, b, a));
			}
		}
	}
}

void DMLWidget::ObjectAppeared()
{
	if (framesWithoutObject >= 5 && framesWithoutObject <= 15)
	{
		ShortAbsence();
	}
	if (framesWithoutObject >= 30 && framesWithoutObject <= 60)
	{
		LongAbsence();
	}
}

void DMLWidget::ShortAbsence()
{
	switch (state)
	{
	case STATE_COLORPALETTE:
		state = STATE_DETECTING;
		break;
	case STATE_DETECTING:
		state = STATE_DRAWING;
		break;
	case STATE_DRAWING:
		state = STATE_DETECTING;
		break;
	default:
		state = STATE_DETECTING;
		break;
	}
}

void DMLWidget::LongAbsence()
{
	if (state != STATE_COLORPALETTE)
	{
		state = STATE_COLORPALETTE;
	}
}

void DMLWidget::ChangeCam(unsigned int nr)
{
	capture = VideoCapture(nr);
}

void DMLWidget::CV2QT(const Mat& img)
{
	switch (img.type())
	{
	case CV_8UC1:
		cvtColor(img, frame, CV_GRAY2RGB);
		break;
	case CV_8UC3:
		cvtColor(img, frame, CV_BGR2RGB);
		break;
	default:
		printf("Unknown OpenCV image type: %d", img.type());
	}

	qtimg = QImage(frame.data, frame.cols, frame.rows, frame.cols * 3, QImage::Format_RGB888);

	if (paints.rect() != qtimg.rect())
	{
		paints = QImage(frame.cols, frame.rows, qtimg.format());
		if (!paints.hasAlphaChannel())
		{
			paints = paints.convertToFormat(QImage::Format::Format_ARGB32);
		}
		paints.fill(Qt::transparent);
	}
	qtimg = qtimg.scaled(this->width(), this->height(), Qt::KeepAspectRatio);
}

void DMLWidget::paint(const std::map<std::pair<int, int>, unsigned int>& pixel_changes)
{
	if (state == STATE_DRAWING)
	{
		for (auto it = pixel_changes.begin(); it != pixel_changes.end(); it++)
		{
			paints.setPixel(it->first.first, it->first.second, it->second);
		}
	}
}

void DMLWidget::paint(int x, int y, int radius, unsigned int color)
{
	if (state == STATE_DRAWING)
	{
		for (int i = x - radius; i < x + radius; i++)
		{
			for (int j = y - radius; j < y + radius; j++)
			{
				if (i < 0) continue;
				if (j < 0) continue;
				if (i > this->width() + this->pos().x()) continue;
				if (j > this->height() + this->pos().y()) continue;
				if (sqrt(pow(x - i, 2) + pow(y - j, 2)) <= radius)
				{
					paints.setPixel(i, j, color);
				}
			}
		}
	}
}

unsigned int RGB2Uint(short r, short g, short b, short a)
{
	if (r > 255) r = 255;
	if (r < 0) r = 0;

	if (g > 255) g = 255;
	if (g < 0) g = 0;

	if (b > 255) b = 255;
	if (b < 0) b = 0;

	if (a > ALPHA_MAX) a = ALPHA_MAX;
	if (a < 0) a = 0;

	unsigned int out = a;
	out <<= 8;
	out += r;
	out <<= 8;
	out += g;
	out <<= 8;
	out += b;
	return out;
}

Brush DMLWidget::Detect()
{
	Mat contourmatrix;
	Brush brush;

	// prepare countour matrix for detection
	cvtColor(frame, contourmatrix, COLOR_RGB2HSV);
	//inRange(contourmatrix, Scalar(90, 200, 50), Scalar(130, 255, 255), contourmatrix);
	inRange(contourmatrix, Scalar(80, 200, 50), Scalar(165, 255, 255), contourmatrix);
	imshow("contour matrix", contourmatrix);
	erode(contourmatrix, contourmatrix, Mat(), Point(-1, -1), 2);
	dilate(contourmatrix, contourmatrix, Mat(), Point(-1, -1), 2);
	//GaussianBlur(contourmatrix, contourmatrix, Size(15, 15), 2, 2);
	//find all contours
	vector<vector<Point> > contours;
	findContours(contourmatrix, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE, Point(0, 0));
	// find max contour
	double largestContourArea = 0;
	//double minContourArea = 3000;
	double minContourArea = frame.rows * frame.cols * 0.005;
	int largestContourIndex = 0;
	Point2f ContourCircleCenter;
	float ContourCircleRadius = 0;
	if (contours.size() > 0) {
		for (int i = 0; i < contours.size(); i++) {
			double currentArea = contourArea(contours[i], false);
			if (currentArea > largestContourArea) {
				largestContourIndex = i;
				largestContourArea = currentArea;
			}
		}
		// get circle of largest contour
		minEnclosingCircle(contours[largestContourIndex], ContourCircleCenter, ContourCircleRadius);
		if (contours[largestContourIndex].size() >= 5)
		{
			RotatedRect ellipRect = fitEllipse(contours[largestContourIndex]);
			float EllipArea = ellipRect.size.width / 2.0 * ellipRect.size.height / 2.0 * 3.14;
			if ((largestContourArea > minContourArea) && (abs(EllipArea - largestContourArea) < 0.1*EllipArea)) {
				if (!objectDetected)
				{
					ObjectAppeared();
				}
				framesWithoutObject = 0;
				objectDetected = true;
				float BrushSizeMultiplier = frame.cols / 4 / ContourCircleRadius;
				brush.center = ContourCircleCenter;
				if (BrushSizeMultiplier < 1) BrushSizeMultiplier = 1;
				brush.radius = ContourCircleRadius / BrushSizeMultiplier;
				brush.found = true;
			}
			else
			{
				objectDetected = false;
				framesWithoutObject++;
			}
		}
	}
	return brush;
}

const QImage DMLWidget::GetImage()
{
	QImage out = paints;
	for (int i = 0; i < out.width(); i++)
	{
		for (int j = 0; j < out.height(); j++)
		{
			QRgb rgba = out.pixel(i, j);
			int r = qRed(rgba);
			int g = qGreen(rgba);
			int b = qBlue(rgba);
			int a = qAlpha(rgba) * 255.0 / ALPHA_MAX;
			out.setPixelColor(QPoint(i, j), QColor(r, g, b, a));
		}
	}
	return out;
}

void DMLWidget::SetImage(const QImage& img)
{
	paints = img.scaled(paints.width(), paints.height());
	for (int i = 0; i < paints.width(); i++)
	{
		for (int j = 0; j < paints.height(); j++)
		{
			QRgb rgba = paints.pixel(i, j);
			int r = qRed(rgba);
			int g = qGreen(rgba);
			int b = qBlue(rgba);
			int a = qAlpha(rgba) / (255.0 / ALPHA_MAX);
			paints.setPixelColor(QPoint(i, j), QColor(r, g, b, a));
		}
	}
}

void DMLWidget::ClearImage()
{
	paints = QImage();
}

DMLWidget::DMLWidget(QWidget* parent) : QWidget(parent), capture(0), state(STATE_DETECTING), objectDetected(true), framesWithoutObject(0), set_color(0, 255, 0)
{
	if (capture.isOpened())
	{
		if (capture.read(frame))
		{
			GenerateColorPalette(min(frame.rows * 0.8f, frame.cols * 0.8f) * 0.5f);
		}
	}
}


DMLWidget::~DMLWidget()
{
}

void DMLWidget::paintEvent(QPaintEvent* event)
{
	if (capture.isOpened())
	{
		if (capture.read(frame))
		{
			Mat mirror;
			flip(frame, mirror, 1);
			CV2QT(mirror);
			QPainter painter(this);
			Brush brush = Detect();
			if (brush.found)
			{
				paint(brush.center.x, brush.center.y, brush.radius, RGB2Uint(set_color.red(), set_color.green(), set_color.blue(), set_color.alpha()));
				QPainter circle(&qtimg);
				switch (state)
				{
				case STATE_DETECTING:
					circle.setPen(Qt::red);
					break;
				case STATE_DRAWING: case STATE_COLORPALETTE:
					QColor tmp = set_color;
					if (tmp.alpha() < 30)
						tmp = Qt::white;
					circle.setPen(tmp);
					break;
				}
				circle.drawEllipse(brush.center.x * (float)qtimg.width() / frame.cols - brush.radius, brush.center.y * (float)qtimg.height() / frame.rows - brush.radius, brush.radius * 2.0, brush.radius * 2.0);
				circle.end();
			}
			painter.drawImage(QPoint((this->width() - qtimg.width()) / 2.0, 0), qtimg);
			if (state == STATE_COLORPALETTE)
			{
				QImage palette_scaled = color_palette.scaled(qtimg.width() * 0.8f, qtimg.height() * 0.8f, Qt::KeepAspectRatio);
				QPoint pos((max(qtimg.width(), this->width()) - palette_scaled.width()) / 2.0, (min(qtimg.height(), this->height()) - palette_scaled.height()) / 2.0);
				painter.drawImage(pos, palette_scaled);
				if (brush.found)
				{
					int radius = palette_scaled.width() * 0.5f;
					float brush_palette_pos_x = brush.center.x * (float)qtimg.width() / frame.cols - 0.5f * qtimg.width();
					float brush_palette_pos_y = brush.center.y * (float)qtimg.height() / frame.rows - 0.5f * qtimg.height();
					if (sqrt(pow(brush_palette_pos_x, 2) + pow(brush_palette_pos_y, 2)) <= radius)
					{
						brush_palette_pos_x += radius;
						brush_palette_pos_y += radius;
						if (brush_palette_pos_x >= 0 && brush_palette_pos_x < palette_scaled.width() && brush_palette_pos_y >= 0 && brush_palette_pos_y < palette_scaled.height())
						{
							QRgb rgba = palette_scaled.pixel(brush_palette_pos_x, brush_palette_pos_y);
							int r = qRed(rgba);
							int g = qGreen(rgba);
							int b = qBlue(rgba);
							int a = qAlpha(rgba) * (255.0 / ALPHA_MAX);
							set_color.setRed(r);
							set_color.setGreen(g);
							set_color.setBlue(b);
							set_color.setAlpha(a);
						}
					}
				}
			}
			else
			{
				QImage paints_scaled = paints.scaled(this->width(), this->height(), Qt::KeepAspectRatio);
				painter.drawImage(QPoint((this->width() - qtimg.width()) / 2.0, 0), paints_scaled);
			}
			painter.end();
		}
		update();
	}
}