#ifndef DMLWIDGET_H
#define DMLWIDGET_H

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/opencv.hpp"

#include "QtWidgets/qwidget.h"
#include "QtGui/qpainter.h"
#include "QtCore/qobject.h"
#include "QtGui/qevent.h"

#include <math.h>

#define ALPHA_MAX 160

using namespace cv;

unsigned int RGB2Uint(short r, short g, short b, short a = ALPHA_MAX);
void Angle2RGBA(float angle, short& r, short& g, short& b, short& a);
void Dist2RGBA(float perc_dist, short &r, short& g, short& b, short& a);
void ColorClamp(short& r, short& g, short& b, short& a);

/**
 * @struct Brush
 * @file DMLWidget.h
 * 
 * Struktura zawierająca informacje o wykrytym znaczniku
 */
struct Brush
{
	/** Informacja o tym czy znacznik został wykryty w obrazie. */
	bool found = false;
	/** Środek wykrytego znacznika. */
	Point2f center;
	/** Promień wykrytego znacznika. */
	float radius;
};

/**
 * @class DMLWidget
 * @file DMLWidget.h
 * 
 * Widget dodawany jako element GUI w klasie DrawMeLikeApp
 * 
 * Główna klasa aplikacji, w której znajduje się większość kodu odpowiedzialnego za realizowane funkcje do których należą: 
 * przechwytywanie obrazu z kamery, 
 * wyświetlanie przechwyconego obrazu, 
 * wyświetlanie obrazu z rysunkiem, 
 * modyfikowanie rysunku, 
 * detekcja znacznika.
 */
class DMLWidget : public QWidget
{
private:
	/** Obiekt wykorzystywany do przechwytywania obrazów z wybranej kamery. */
	VideoCapture capture;
	/** Przechwycona klatka obrazu z kamery. */
	Mat frame;
	/** Obraz z kamery w postaci obrazu akceptowalnego przez bibliotekę Qt */
	QImage qtimg;
	/** Rysunek który modyfikuje użytkownik */
	QImage paints;
	/** Kolor pędzla. */
	QColor set_color;
	/** Generacja palety kolorów */
	void GenerateColorPalette(int radius);
	/** Obraz reprezentujący paletę kolorów */
	QImage color_palette;
	/** Możliwe stany w których znajduje się aplikacja. */
	enum
	{
		STATE_DETECTING,
		STATE_DRAWING,
		STATE_COLORPALETTE
	} state;
	/** Informacja o tym, czy znacznik został wykryty podczas poprzedniej detekcji */
	bool objectDetected;
	/** Liczba klatek w których znacznik nie został wykryty */
	unsigned int framesWithoutObject;
	/** Metoda wywoływana gdy znacznik pojawi się na obrazie. */
	void ObjectAppeared();
	/** Metoda wywoływana w przypadku krótkiej nieobecności znacznika na obrazie. */
	void ShortAbsence();
	/** Metoda wywoływana w przypadku długiej nieobecności znacznika na obrazie. */
	void LongAbsence();
public:
/**
 * @brief Metoda zwracająca zalecany rozmiar widgetu.
 * @return Zalecany rozmiar widgetu.
 */
	QSize sizeHint() const { return qtimg.size(); }
/**
 * @brief Metoda zwracająca minimalny rozmiar widgetu.
 * @return Minimalny rozmiar widgetu.
 */
	QSize minimumSizeHint() const { return qtimg.size(); }

/**
 * @brief Zmiana kamery wykorzystywanej przez obiekt capture
 * @param nr nr kamery
 */
	void ChangeCam(unsigned int nr);

/**
 * @brief Konwersja obrazu z OpenCV do Qt, obraz powstały w wyniku działania metody zapisywany jest w obiekcie qtimg.
 * @param img konwertowany obraz OpenCV
 */
	void CV2QT(const Mat& img);

/**
 * @brief Zmiana obrazu po którym rysuje użytkownik
 * @param pixel_changes mapa, w której kluczem jest para określająca piksel, który należy zmienić, a wartością jest kolor który należy nadać pikselowi z klucza
 */
	void paint(const std::map<std::pair<int, int>, unsigned int>& pixel_changes);
/**
 * @brief Zmiana obrazu po którym rysuje użytkownik - wersja z rysowaniem koła
 * @param x składowa x położenia środka rysowanego koła
 * @param y składowa y położenia środka rysowanego koła
 * @param radius promień koła
 * @param color kolor którym należy wypełnić dane koło
 */
	void paint(int x, int y, int radius, unsigned int color = 0);

/**
 * @brief Wykrywanie znacznika na obrazie.
 * @retval Brush wykryty znacznik
 */
	Brush Detect();

/**
 * @brief Metoda wykorzystywana w przypadku zapisu obrazu do pliku.
 * 
 * Dokonuje zmiany w kanale alfa na całym obrazie, dzięki czemu zapisany obraz nie jest przeźroczysty.
 */
	const QImage GetImage();
/**
 * @brief Metoda wykorzystywana w przypadku odczytu obrazu z pliku.
 * 
 * Dokonuje zmiany w kanale alfa na całym obrazie, dzięki czemu wczytany obraz ma maksymalną ustaloną wcześniej wartość kanału alfa.
 * Ponadto skaluje obraz do rozmiarów przechwytywanego przez kamerę obrazu.
 * @param img odczytany obraz
 */
	void SetImage(const QImage& img);
/**
 * @brief Czyszczenie obrazu który narysował użytkownik.
 */
	void ClearImage();

/**
 * @brief Konstruktor klasy DMLWidget
 * 
 * 
 * Jeśli udało się poprawnie przechwycić klatkę z domyślnej kamery, to zostanie wygenerowana paleta kolorów.
 * @param parent rodzic danego widgetu (widget w którym się on znajduje)
 */
	DMLWidget(QWidget* parent = nullptr);
	~DMLWidget();
protected:
/**
 * @brief Przeciążona metoda odziedziczona po klasie QWidget.
 * 
 * Umieszczone są tutaj wszelkie operacje wymagane do narysowania widgetu.
 * @param event argument dostarczany przez aplikację Qt
 */
	void paintEvent(QPaintEvent* event);
};

#endif

