#include "DrawMeLikeApp.hpp"



DrawMeLikeApp::DrawMeLikeApp()
{
	QWidget* central_widget = new QWidget();
	QMenu* img_menu = new QMenu("Obraz");
	QVBoxLayout* layout_v1 = new QVBoxLayout();

	menu_bar.addMenu(img_menu);

	QAction* new_img = new QAction("Nowy obraz", &menu_bar);
	new_img->setShortcuts(QKeySequence::New);
	connect(new_img, &QAction::triggered, [&]() {
		cam.ClearImage();
	});
	QAction* save_img = new QAction("Zapisz obraz", &menu_bar);
	save_img->setShortcuts(QKeySequence::Save);
	connect(save_img, &QAction::triggered, [&]() {
		std::string filename = QFileDialog::getSaveFileName(this, "Zapisz obraz", "", "Obrazy (*.png)").toStdString();
		if (filename != "")
		{
			cam.GetImage().save(QString(filename.c_str()));
		}
	});
	QAction* load_img = new QAction("Wczytaj obraz", &menu_bar);
	load_img->setShortcuts(QKeySequence::Open);
	connect(load_img, &QAction::triggered, [&]() {
		std::string filename = QFileDialog::getOpenFileName(this, "Wczytaj obraz", "", "Obrazy (*.png)").toStdString();
		if (filename != "")
		{
			QImage img;
			img.load(QString(filename.c_str()));
			cam.SetImage(img);
		}
	});

	img_menu->addAction(new_img);
	img_menu->addAction(load_img);
	img_menu->addAction(save_img);

	layout_v1->addWidget(&menu_bar);
	layout_v1->addWidget(&cam);
	//layout_v1->addWidget(&cam_list);

	menu_bar.setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
	layout_v1->setMargin(0);

	//cam_list.addItem("test1");

	central_widget->setLayout(layout_v1);
	this->setCentralWidget(central_widget);
	this->resize(cam.width(), cam.height());
}

DrawMeLikeApp::~DrawMeLikeApp()
{
}
