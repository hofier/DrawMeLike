#include "opencv2/highgui/highgui.hpp"
#include "QtWidgets/qapplication.h"
#include "DrawMeLikeApp.hpp"
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);
	DrawMeLikeApp dmla;

	dmla.show();

	return app.exec();
}