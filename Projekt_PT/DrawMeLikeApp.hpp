#ifndef DRAWMELIKEAPP_HPP
#define DRAWMELIKEAPP_HPP

#include "QtWidgets/qmainwindow.h"
#include "QtWidgets/qcombobox.h"
#include "QtWidgets/qboxlayout.h"
#include "QtWidgets/qlabel.h"
#include "QtWidgets/qmenubar.h"
#include "QtWidgets/qmenu.h"
#include "QtWidgets/qfiledialog.h"
#include "DMLWidget.h"

/**
 * @class DrawMeLikeApp
 * @file DrawMeLikeApp.hpp
 */
class DrawMeLikeApp : public QMainWindow
{
private:
	//QComboBox cam_list;
	DMLWidget cam;
	QMenuBar menu_bar;
public:
	DrawMeLikeApp();
	~DrawMeLikeApp();
};

#endif

